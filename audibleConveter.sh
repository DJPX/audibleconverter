#!/bin/bash
targetfile="$1"
inAudibleNG_tables=tables
kleberj_aax2mp3=aax2mp3
Programfolder="AudibleConverter"
mkdir "$Programfolder"
cd "$Programfolder"



# Install dependencys
# Fedora
sudo dnf install ffmpeg

if [ "$#" -ne 1 ]
then
    echo "Wrong usage. Please use:"
    echo "$0 [pathToFile]"
else
    echo Checking dependencys
    if [ ! -d "$inAudibleNG_tables" ]
    then
        git clone https://github.com/inAudible-NG/tables.git
    fi
    if [ ! -d "$kleberj_aax2mp3" ]
    then
        git clone https://bitbucket.org/kleberj/aax2mp3.git
        chmod +x "$kleberj_aax2mp3/aax2mp3"
    fi
    Checksum=$(ffprobe "../${targetfile}" 2>&1 | grep "\[aax\] file checksum ==" | awk -F 'checksum == ' '{print $2}')
    echo Checksum: $Checksum
    cd "$inAudibleNG_tables"
    hexcode=$(./rcrack . -h "$Checksum" | grep "hex:" | awk -F 'hex:' ' {print $2}')
    echo $hexcode
    cd ..
    aax2mp3/aax2mp3 -i "../$targetfile" -a "$hexcode"
    # cd ../aax2mp3
    # aax2mp3 -i "../${kleberj_aax2mp3}/${targetfile}" -a CAFED00D 
fi


Checksum=$(ffprobe "$targetfile" 2>&1 | grep "\[aax\] file checksum ==" )